package hr.atos.praksa.project01.uilayer;

import java.util.Date;
import java.util.List;

import java.util.ArrayList;
import hr.atos.praksa.project01.businesslogic.*;

public class SensorManagement {
	
	private static SensorAdministration sensorAdministration = new SensorAdministration();
	private static ReadingAdministration readingAdministration = new ReadingAdministration();

    /**
     * Creates new sensor and returns it.
     * 
     * @param name - name of the new sensor.
     * @param type - type of the new sensor.
     * @return Newly created sensor
     */
    public Sensor createSensor(String name, String type) {
        try {
            Sensor sensor = new Sensor(name, type);
            sensorAdministration.saveSensorToDatabase(sensor);
            return sensor;
        } catch (Exception e) {
        	System.err.println(e.getMessage());
        }
        return new Sensor("N/A", "N/A");
    }

    /**
     * Gets sensor data from database by ID.
     * 
     * @param sensorId - ID of the sensor in the database
     * @return Sensor data specified by ID.
     */
    public Sensor getSensor(int sensorId) {
        try {
            Sensor sensor = sensorAdministration.loadSensorFromDatabase(sensorId);
            if (sensor == null) {
                throw new Exception("Cannot get sensor with ID of " + String.valueOf(sensorId) + "!");
            }
            return sensor;
        } catch (Exception e) {
        	System.err.println(e.getMessage());
        }
        return new Sensor("N/A", "N/A");
    }

    /**
     * Returns all sensors from database.
     * 
     * @return All sensors in database.
     */
    public List<Sensor> getAllSensors() {
        try {
            List<Sensor> sensors = sensorAdministration.loadAllSensorsFromDatabase();
            return sensors;
        } catch (Exception e) {
        	System.err.println(e.getMessage());
        }
        return new ArrayList<Sensor>();
    }

    /**
     * Returns a list of sensors in given time interval.
     * 
     * @param beginDate - the first day to look for data.
     * @param endDate   - the last day to look for data.
     * @return All sensors in given time interval.
     */
    public List<Reading> getSensorsInTimeInterval(Date beginDate, Date endDate) {
        try {
            List<Reading> reading = readingAdministration.loadReadingFromDatabase(beginDate, endDate);
            return reading;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return new ArrayList<Reading>();
    }

    /**
     * Creates new reading data and returns it.
     * 
     * @param sensorId - ID of the sensor which provided reading.
     * @param data     - reading data
     * @return Newly created reading.
     */
    public Reading createReading(int sensorId, String data) {
        try {
            Reading reading = new Reading(sensorId, data);
            readingAdministration.saveReadingToDatabase(reading);
            return reading;
        } catch (Exception e) {
        	System.err.println(e.getMessage());
        }
        return new Reading(0, "N/A");
    }

    /**
     * Gets reading data from the database by its ID.
     * 
     * @param readingId - ID of the reading in database.
     * @return Reading data specified by ID.
     */
    public Reading getReading(int readingId) {
        try {
            Reading reading = readingAdministration.loadReadingFromDatabase(readingId);
            if (reading == null) {
                throw new Exception("Cannot get reading with ID of " + String.valueOf(readingId) + "!");
            }
            return reading;
        } catch (Exception e) {
        	System.err.println(e.getMessage());
        }
        return new Reading(0, "N/A");
    }

    /**
     * Returns all readings from database.
     * 
     * @return All readings in database.
     */
    public List<Reading> getAllReadings() {
        try {
            List<Reading> readings = readingAdministration.loadAllReadingFromDatabase();
            return readings;
        } catch (Exception e) {
        	System.err.println(e.getMessage());
        }
        return new ArrayList<Reading>();
    }

    /**
     * Returns a list of readings in given time interval.
     * 
     * @param beginDate - the first day to look for data.
     * @param endDate   - the last day to look for data.
     * @return All readings in given time interval.
     */
    public List<Reading> getReadingsInTimeInterval(Date beginDate, Date endDate) {
        try {
            List<Reading> readings = readingAdministration.loadReadingFromDatabase(beginDate, endDate);
            return readings;
        } catch (Exception e) {
        	System.err.println(e.getMessage());
        }
        return new ArrayList<Reading>();
    }
    
    /**
     * Updates sensor data from database by ID.
     * 
     * @param sensorId - ID of the sensor in the database
     * @param updatedSensor - Sensor data which will be used for updating
     * @return true if update was successful.
     */
    public boolean updateSensor(int sensorId, Sensor updatedSensor) {
        try {
            sensorAdministration.updateSensorInDatabase(sensorId, updatedSensor);
            return true;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return false;
    }
    
    /**
     * Deletes sensor data from database by ID.
     *
     * @param sensorId - ID of the sensor in the database
     * @return true if delete process was successful.
     */
    public boolean deleteSensor(int sensorId) {
        try {
            sensorAdministration.removeSensorFromDatabase(sensorId);
            return true;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
		return false;
    }
}