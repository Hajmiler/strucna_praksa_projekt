package hr.atos.praksa.project01.dblayer;

import hr.atos.praksa.project01.exception.BackendException;

public class DataNotFoundException extends BackendException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4437774408166696499L;

	public DataNotFoundException(String message, int code) {
		super(message, code);
	}

}
