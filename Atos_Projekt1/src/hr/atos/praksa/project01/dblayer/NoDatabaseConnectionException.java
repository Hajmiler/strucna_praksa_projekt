package hr.atos.praksa.project01.dblayer;

import hr.atos.praksa.project01.exception.BackendException;

public class NoDatabaseConnectionException extends BackendException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6965445661173370569L;

	public NoDatabaseConnectionException(String message, Throwable cause, int code) {
		super(message, cause, code);
	}

}
