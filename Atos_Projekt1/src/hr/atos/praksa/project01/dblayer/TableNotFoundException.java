package hr.atos.praksa.project01.dblayer;

import hr.atos.praksa.project01.exception.BackendException;

public class TableNotFoundException extends BackendException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1756598849656918826L;

	public TableNotFoundException(String message, Throwable cause, int code) {
		super(message, cause, code);
	}

}
