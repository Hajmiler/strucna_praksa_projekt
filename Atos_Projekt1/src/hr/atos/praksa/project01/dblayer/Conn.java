package hr.atos.praksa.project01.dblayer;

import java.sql.Connection;
import java.sql.DriverManager;

import hr.atos.praksa.project01.exception.BackendException;

public class Conn {
	
	private static Connection connection;
	
	public static Connection getConnection() throws BackendException {
		if(connection != null)
			return connection;
		else {
			try {
				Class.forName("org.sqlite.JDBC");
				connection = DriverManager.getConnection("jdbc:sqlite:c:\\tomcat\\db\\Project01.db");
			}catch(Exception e) {
				throw new NoDatabaseConnectionException("It is not possible to merge to a database.", e, 108);
			}
			return connection;
		}
	}
}
