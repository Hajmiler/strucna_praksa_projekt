package hr.atos.praksa.project01.dblayer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import hr.atos.praksa.project01.businesslogic.Reading;
import hr.atos.praksa.project01.businesslogic.Sensor;
import hr.atos.praksa.project01.exception.BackendException;

import java.util.Date;
import java.util.ArrayList;
import java.sql.Statement;
import java.text.SimpleDateFormat;

public class DatabaseAccessLayer {

	private Connection connection;
	protected String name;
	protected String type;
	protected String sql;
	protected Integer sensorId;
	protected Date date;
	protected String data;
	
	public DatabaseAccessLayer() throws BackendException {
		connection = Conn.getConnection();
	}
	
	public void createSensor(Sensor sensor) throws BackendException {
		
		try {
			name = sensor.getName();
			type = sensor.getType();

			String sql = "INSERT INTO Sensor (name, type) VALUES (?, ?) ";

			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			
			preparedStatement.setString(1, name);
			preparedStatement.setString(2, type);

			int results = preparedStatement.executeUpdate();
			
			if(results == 0) 
				throw new DataNotFoundException("The sensors is not exist.", 106);

		}catch(SQLException e) {
			throw new TableNotFoundException("There is no such sensor table in the database.", e, 107);
		}
	}
	
	public List<Sensor> readAllSensors() throws BackendException{
		List<Sensor> sensors = new ArrayList<Sensor>();
		
		try {
			Statement statement = connection.createStatement();
			String sql = "SELECT * FROM Sensor;";
				
			ResultSet results = statement.executeQuery(sql);
			if(results.isBeforeFirst()) {
				while(results.next()) {
					Sensor sensor = new Sensor();
		
					sensor.setName(results.getString("name"));
					sensor.setType(results.getString("type"));
						
					sensors.add(sensor);
				}
			}
			else {
				throw new DataNotFoundException("The sensors is not exist.", 106);
			}
		} catch(SQLException e) {
			throw new TableNotFoundException("There is no such sensor table in the database.", e, 107);
		}
			
		return sensors;
	}
	
	public Sensor readSensor(int id) throws BackendException {
		Sensor sensor = new Sensor();
		
		try {
			sql = "SELECT * FROM Sensor WHERE id= ? ;";
			
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			
			preparedStatement.setInt(1, id);
			
			ResultSet results = preparedStatement.executeQuery();
			
			if(results.next()) {
				sensor.setName(results.getString("name"));
				sensor.setType(results.getString("type"));
			}
			else {
				throw new DataNotFoundException("The sensors is not exist.", 106);
			}

		}catch(SQLException e) {
			throw new TableNotFoundException("There is no such sensor table in the database.", e, 107);
		}
		
		return sensor;
	}
	
	public void updateSensor(int sensorId, Sensor sensor) throws BackendException {
		try {
			sql = "UPDATE Sensor";
			sql += " SET name = ? , type = ? ";
			sql += " WHERE id = ? ";
			
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			
			preparedStatement.setString(1, sensor.getName());
			preparedStatement.setString(2, sensor.getType());
			preparedStatement.setInt(3, sensorId);
			
			int results = preparedStatement.executeUpdate();
			
			if(results == 0) 
				throw new DataNotFoundException("The sensors is not exist.", 106);
			
		}catch(SQLException e) {
			throw new TableNotFoundException("There is no such sensor table in the database.", e, 107);	
		}
	}
	
	public void deleteSensor(int id) throws BackendException { 
		try {
			sql = "DELETE FROM Sensor WHERE id= ?;";
			
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			
			preparedStatement.setInt(1, id);
			
			int results = preparedStatement.executeUpdate();
			
			if(results == 0) 
				throw new DataNotFoundException("The sensors is not exist.", 106);

		}catch(SQLException e) {
			throw new TableNotFoundException("There is no such sensor table in the database.", e, 107);	
		}
	}
	
	public void createReading(Reading reading) throws BackendException {
		
		try {
			sensorId = reading.getSensorId();
			data = reading.getData();
			date = reading.getDate();
		
			sql = "INSERT INTO Reading (sensorId, date, data) VALUES (?, ?, ?) ";

			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			
			SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-DD HH:MM:SS.SSS");
			
			preparedStatement.setInt(1,  sensorId.intValue());
			preparedStatement.setString(2, sdf.format(date.getTime()));
			preparedStatement.setString(3, data);

			int results = preparedStatement.executeUpdate();
			
			if(results == 0) 
				throw new DataNotFoundException("The reading is not exist.", 106);
		}catch(SQLException e) {
			throw new TableNotFoundException("There is no such reading table in the database.", e, 107);	
		}
	}
	
	public List<Reading> readAllReadings() throws BackendException {
		List<Reading> readings = new ArrayList<Reading>();
		try {
			Statement statement = connection.createStatement();
			
			String sql = "SELECT * FROM Reading;";
			
			ResultSet results = statement.executeQuery(sql);
			
			if(results.isBeforeFirst()) {
				while(results.next()) {
					Reading reading = new Reading();
		
					reading.setSensorId(results.getInt("sensorId"));
					reading.setData(results.getString("data"));
					readings.add(reading);
				}
			}
			else {
				throw new DataNotFoundException("The reading is not exist.", 106);
			}
		}catch(SQLException e) {
			throw new TableNotFoundException("There is no such reading table in the database.", e, 107);	
		}
		
		return readings;
	}
	
	public Reading readReading(int id) throws BackendException {  
		Reading reading = new Reading();
		
		try {
			sql = "SELECT * FROM Reading WHERE id= ? ;";
			
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			
			preparedStatement.setInt(1, id);
			
			ResultSet results = preparedStatement.executeQuery();
			
			if(results.next()) {
				reading.setSensorId(results.getInt("sensorId"));
				reading.setData(results.getString("data"));
			}
			else {
				throw new DataNotFoundException("The reading is not exist.", 106);
			}
		}catch(SQLException e) {
			throw new TableNotFoundException("There is no such reading table in the database.", e, 107);	
		}
		
		return reading;
	}
		
	public List<Reading> readReading(Date startDate, Date endDate) throws BackendException{
		SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-DD");
		String strStartDate = sdf.format(startDate);
		String strEndDate = sdf.format(endDate);
		
		List<Reading> readings = new ArrayList<Reading>();
		try {
			Statement statement = connection.createStatement();
			
			String sql = "SELECT * FROM Reading WHERE date >= \"" + strStartDate + "\" AND date <= \"" + strEndDate + "\";";

			ResultSet results = statement.executeQuery(sql);
			if(results.isBeforeFirst()) {
				while(results.next()) {
					Reading reading = new Reading();
					reading.setSensorId(results.getInt("sensorId"));
					reading.setData(results.getString("data"));
					readings.add(reading);
				}
			}
			else {
				throw new DataNotFoundException("The reading is not exist.", 106);
			}
		}catch(SQLException e) {
			throw new TableNotFoundException("There is no such reading table in the database.", e, 107);	
		}
		
		return readings;
	}
}