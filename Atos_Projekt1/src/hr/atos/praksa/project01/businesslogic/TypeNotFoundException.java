package hr.atos.praksa.project01.businesslogic;

import hr.atos.praksa.project01.exception.BackendException;

public class TypeNotFoundException extends BackendException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5996816423833593164L;

	public TypeNotFoundException(String message, int code) {
		super(message, code);
	}

}
