package hr.atos.praksa.project01.businesslogic;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import hr.atos.praksa.project01.dblayer.DatabaseAccessLayer;
import hr.atos.praksa.project01.exception.BackendException;

public class ReadingAdministration extends Reading {
	private DatabaseAccessLayer database;
	
	public ReadingAdministration() throws BackendException {
		database = new DatabaseAccessLayer();
	}

	public boolean checkReading(Reading reading) throws BackendException {
		if (reading.getData().isEmpty()) {
			throw new DataNotFoundException("The sensor has not readings.", 104);
		}
		if (reading.getData().length() > 500) {
			throw new TooManyCharException("The reading data contains more then 500 characher.", 102);
		}
		return true;
	}

	public void saveReadingToDatabase(Reading reading) throws BackendException {
		if (checkReading(reading)) {
			database.createReading(reading);
		}
	}

	public Reading loadReadingFromDatabase(int readingId) throws BackendException {
		Reading reading = database.readReading(readingId);
		if (reading != null) 
			return reading;
		
		return null;
	}

	public List<Reading> loadReadingFromDatabase(Date startDate, Date endDate) throws BackendException {
		Date currentDate = new Date();
			
		if (startDate.before(endDate)) {
			if(!endDate.after(currentDate)) {
				List<Reading> readings = new ArrayList<>();
				readings = database.readReading(startDate, endDate);
				return readings;
			} else
				throw new DateNotRealException("The end date is bigger than the current date.", 105);
		} else 
			throw new DateNotRealException("The begin date is bigger than the end date.", 105);
	}

	public List<Reading> loadAllReadingFromDatabase() throws BackendException {
		List<Reading> readings = new ArrayList<>();
			
		readings = database.readAllReadings();
		if (readings != null) 
			return readings;

		return null;
	}
}
