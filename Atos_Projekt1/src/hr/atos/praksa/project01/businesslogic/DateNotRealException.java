package hr.atos.praksa.project01.businesslogic;

import hr.atos.praksa.project01.exception.BackendException;

public class DateNotRealException extends BackendException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5733498404153703917L;

	public DateNotRealException(String message, int code) {
		super(message, code);
	}

}
