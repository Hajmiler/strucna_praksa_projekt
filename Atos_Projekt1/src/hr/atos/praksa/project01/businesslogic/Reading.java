package hr.atos.praksa.project01.businesslogic;

import java.util.Date;

public class Reading {
	private int id, sensorId;
	private Date date;
	private String data;
	
	public Reading() {
		date = new Date();
		data = null;
	}
	
	public Reading(int sensorId, String data) {
		this.sensorId = sensorId;
		this.data = data;
		this.date = new Date();
	}
	
	public int getId() {
		return id;
	}
	public int getSensorId() {
		return sensorId;
	}
	public void setSensorId(int sensorId) {
		this.sensorId = sensorId;
	}
	public Date getDate() {
		return date;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}	
}
