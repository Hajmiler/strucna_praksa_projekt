package hr.atos.praksa.project01.businesslogic;

public class Sensor {
	private int id;
	private String name;
	private String type;
	
	public Sensor() {
		name = null;
		type = null;
	}
	
	public Sensor(String name, String type) {
		this.name = name;
		this.type = type;
	}
	
	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}
