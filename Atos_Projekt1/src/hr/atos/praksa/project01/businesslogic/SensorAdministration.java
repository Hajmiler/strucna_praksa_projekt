package hr.atos.praksa.project01.businesslogic;

import java.util.ArrayList;
import java.util.List;

import hr.atos.praksa.project01.dblayer.DatabaseAccessLayer;
import hr.atos.praksa.project01.exception.BackendException;

public class SensorAdministration extends Sensor{
	private DatabaseAccessLayer database;
	
	public SensorAdministration() throws BackendException {
		database = new DatabaseAccessLayer();
	}

	public boolean checkSensor(Sensor sensor) throws BackendException {
		if(sensor.getName().equals("")) {
			throw new NameNotFoundException("The sensor name was not entered.", 101);
		}
		else if(sensor.getName().length() > 100) {
			throw new TooManyCharException("The sensor name contains more then 100 characher.", 102);
		}
		if(sensor.getType().equals("")) {
			throw new TypeNotFoundException("The sensor type was not entered.", 103);
		}
		else if(sensor.getType().length() > 100) {
			throw new TooManyCharException("The sensor type contains more then 100 characher.", 102);
		}
		return true;
	}

	public void saveSensorToDatabase(Sensor sensor) throws BackendException {
		if (checkSensor(sensor)) {
			database.createSensor(sensor);
		}
	}

	public Sensor loadSensorFromDatabase(int sensorID) throws BackendException {
		Sensor sensor = database.readSensor(sensorID);
		if (sensor != null) {
			return sensor;
		}

		return null;
	}

	public List<Sensor> loadAllSensorsFromDatabase() throws BackendException {
		List<Sensor> sensors = new ArrayList<>();
			
		sensors = database.readAllSensors();
		if (sensors != null) {
			return sensors;
		}

		return null;
	}

	public void updateSensorInDatabase(int sensorId, Sensor sensor) throws BackendException {
		if (checkSensor(sensor)) {
			database.updateSensor(sensorId, sensor);
		}	
	}

	public void removeSensorFromDatabase(int sensorId) throws BackendException {
		database.deleteSensor(sensorId);
	}
}
