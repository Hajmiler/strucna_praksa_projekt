package hr.atos.praksa.project01.businesslogic;

import hr.atos.praksa.project01.exception.BackendException;

public class DataNotFoundException extends BackendException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8098998931413288753L;

	public DataNotFoundException(String message, int code) {
		super(message, code);
	}

}
