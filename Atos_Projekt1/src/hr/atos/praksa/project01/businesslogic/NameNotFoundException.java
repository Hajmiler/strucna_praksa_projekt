package hr.atos.praksa.project01.businesslogic;

import hr.atos.praksa.project01.exception.BackendException;

public class NameNotFoundException extends BackendException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7385740180731821130L;

	public NameNotFoundException(String message, int code) {
		super(message, code);
	}

}
