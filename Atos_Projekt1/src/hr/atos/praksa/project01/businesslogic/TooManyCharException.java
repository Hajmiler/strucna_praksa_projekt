package hr.atos.praksa.project01.businesslogic;

import hr.atos.praksa.project01.exception.BackendException;

public class TooManyCharException extends BackendException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6388467308411277779L;

	public TooManyCharException(String message, int code) {
		super(message, code);
	}

}
