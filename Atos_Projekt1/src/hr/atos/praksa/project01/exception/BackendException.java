package hr.atos.praksa.project01.exception;

public class BackendException extends Exception {

	private static final long serialVersionUID = -8460356990632230194L;

	private final int errorCode;

	public BackendException(int code) {
		super();
		this.errorCode = code;
	}

	public BackendException(String message, Throwable cause, int code) {
		super(message, cause);
		this.errorCode = code;
	}

	public BackendException(String message, int code) {
		super(message);
		this.errorCode = code;
	}

	public BackendException(Throwable cause, int code) {
		super(cause);
		this.errorCode = code;
	}
	
	public int getCode() {
		return this.errorCode;
	}
}